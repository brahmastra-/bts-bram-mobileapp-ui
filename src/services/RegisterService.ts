import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http'
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';


@Injectable()
export class RegisterService {
    BASE_URL="http://13.235.239.238:9007";

    //BA
    
  constructor(  public http: Http){
  
  }

  registerFunction(data: any)  {

    let headers = new Headers ({ 'Content-Type': 'application/json',
    'tenantIdHeader': 0
  });

  let options = new RequestOptions({ headers: headers, method: "post" });
 return this.http.post(this.BASE_URL +'/userSignup' , data, options);
    }
  
}
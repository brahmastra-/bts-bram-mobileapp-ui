import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import 'rxjs/Rx';


@Injectable()
export class AlertService {
  BASE_URL="http://13.235.239.238:9007";

    //BASE_URL="http://10.0.2.122";
    
    

    
  constructor(  public http: Http){
  
  }

  save(data: any)  {

    let headers = new Headers ({ 'Content-Type': 'application/json',
    'tenantIdHeader': 0
  });

  let options = new RequestOptions({ headers: headers, method: "post" });
 return this.http.post(this.BASE_URL +'/addAlert' , data, options);
        }

  getMyAlert(nUserId)  {
       return this.http.get(this.BASE_URL +'/getAllAlerts?nUserId='+nUserId);
  }
}
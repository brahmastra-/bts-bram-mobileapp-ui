import { Component, ViewChild } from "@angular/core";
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Nav, Platform } from "ionic-angular";
import { AlertPage } from "../pages/alert/alert";
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { ShowAlertPage } from "../pages/showalert/showalert";








export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    //this.initializeApp();

    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Alert', component: AlertPage, icon: 'partly-sunny'},
      {title: 'Show Alert', component: ShowAlertPage, icon: 'partly-sunny'},
      
      
      //{title: 'Local Weather', component: LocalWeatherPage, icon: 'partly-sunny'}
    ];
  }

  // initializeApp() {
  //   this.platform.ready().then(() => {
  //     // Okay, so the platform is ready and our plugins are available.

  //     //*** Control Splash Screen
  //     // this.splashScreen.show();
  //     // this.splashScreen.hide();

  //     //*** Control Status Bar
  //     this.statusBar.styleDefault();
  //     this.statusBar.overlaysWebView(false);

  //     //*** Control Keyboard
  //    // this.keyboard.disableScroll(true);
  //   });
  // }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.nav.setRoot(LoginPage);
  }

}

import {Component} from "@angular/core";
import {NavController, PopoverController, NavParams} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {AlertPage} from "../alert/alert";
import {ShowAlertPage} from "../showalert/showalert";

import {NotificationsPage} from "../notifications/notifications";
import {SettingsPage} from "../settings/settings";
import {TripsPage} from "../trips/trips";
import {SearchLocationPage} from "../search-location/search-location";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {


  private userId;
  // search condition
  public search = {
    name: "Rio de Janeiro, Brazil",
    date: new Date().toISOString()
  }

  constructor(private storage: Storage, public nav: NavController, 
    public popoverCtrl: PopoverController,
    public navParams: NavParams) {
      this.userId = navParams.get('key');
  }

  
  onSelectChange(selectedValue: any) {
    console.log('Selected', selectedValue, this.userId);
    
    if(selectedValue == "1"){

      this.nav.push(AlertPage, {key  : this.userId});
      

    }else if(selectedValue == "2"){

      this.nav.push(ShowAlertPage);
      
    }
  }

  ionViewWillEnter() {
    // this.search.pickup = "Rio de Janeiro, Brazil";
    // this.search.dropOff = "Same as pickup";
    this.storage.get('pickup').then((val) => {
      if (val === null) {
        this.search.name = "Rio de Janeiro, Brazil"
      } else {
        this.search.name = val;
      }
    }).catch((err) => {
      console.log(err)
    });
  }

  // go to result page
  doSearch() {
    this.nav.push(TripsPage);
  }

  // choose place
  choosePlace(from) {
    this.nav.push(SearchLocationPage, from);
  }

  // to go account page
  goToAccount() {
    this.nav.push(SettingsPage);
  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: myEvent
    });
  }

}

//

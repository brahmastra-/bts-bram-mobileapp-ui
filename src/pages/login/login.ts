import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController, MenuController, NavController, ToastController } from "ionic-angular";
import { LoginService } from '../../services/LoginService';
import { HomePage } from "../home/home";
import { RegisterPage } from "../register/register";




@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {


  private userID;
  validations_form: FormGroup;

  constructor(public nav: NavController, public forgotCtrl: AlertController,  public loginService: LoginService,    public formBuilder: FormBuilder,
    public menu: MenuController, public toastCtrl: ToastController) {
    this.menu.swipeEnable(false);
  }



  ionViewWillLoad() {

        this.validations_form = this.formBuilder.group({
          sEmailIdOrnMobileNo: new FormControl('', Validators.compose([
            Validators.maxLength(25),
            Validators.minLength(2),
            Validators.required
          ])),



        });


      }

  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }

  // login and go to home page
  login(value): void{
    console.log("data",this.validations_form.value.sEmailIdOrnMobileNo);
    console.log("data",value.sEmailIdOrnMobileNo);
    if(this.validations_form.value.sEmailIdOrnMobileNo == ""){
      console.log("data",this.validations_form.value);

    }else{
      this.loginService.loginFunction(this.validations_form.value.sEmailIdOrnMobileNo).subscribe(res => {
          
        let data = JSON.parse(res['_body']);
         console.log(data.data.nUserId);

          // console.log(res['data'].nUserId);
          this.userID =  data.data.nUserId;

          // console.log(this.usredata);
          console.log(" this.userID",  +this.userID);
          this.nav.push(HomePage, {key  : this.userID});
        }
      );
      console.log("data",this.validations_form.value.sEmailIdOrnMobileNo);

      // this.nav.setRoot(HomePage,  {myVal: this.userID} );

     
    }

  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}

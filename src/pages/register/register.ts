import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {LoginPage} from "../login/login";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RegisterService } from '../../services/RegisterService';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {


  validations_form: FormGroup;
  
  constructor(public nav: NavController,public regService: RegisterService,      public formBuilder: FormBuilder) {
  }



  ionViewWillLoad() {
    
        this.validations_form = this.formBuilder.group({
          sName: new FormControl('', Validators.compose([
            Validators.maxLength(25),
            Validators.minLength(2),
            Validators.required
          ])),
    
          sEmailId: new FormControl('', Validators.compose([
            Validators.maxLength(25),
            Validators.minLength(2),
            Validators.required
          ])),

          nMobileNo: new FormControl('', Validators.compose([
            Validators.maxLength(25),
            Validators.minLength(2),
            Validators.required
          ])),
    
        });
        
    
      }

  // register and go to home page
  register(value: string): void {

    // {
    //   "sEmailId":"saudagar@gmail.com",
    //     "nMobileNo":9999999,
    //      "sName":"ajay"
    //   }

    console.log("Payload Signup....",this.validations_form.value)
    this.regService.registerFunction(value)
    .subscribe(res => {
      console.log(res.json());
      // let alert = this.alertCtrl.create({
      //   subTitle: "Registration Successfull",
      //   buttons: [{
      //     text: 'OK',
      //     handler: () => {

      //       this.navCtrl.setRoot(LoginPage);
      //     }
      //   }]
      // });
    //  alert.present();
      //this.validations_form.reset();
    }, (err) => {

        // var error = JSON.parse(err._body);
        // var errMsg = error.message;
        // let alert = this.alertCtrl.create({
        //   subTitle: errMsg,
        //   buttons: ['OK']
        // });
        // alert.present();
    });

    
    this.nav.setRoot(LoginPage);
  }

  
}

import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { AlertService } from '../../services/AlertService';
import { HomePage } from "../home/home";
import { NotificationsPage } from "../notifications/notifications";
import { SearchLocationPage } from "../search-location/search-location";
import { SettingsPage } from "../settings/settings";
import { TripsPage } from "../trips/trips";



@Component({
  selector: 'page-home',
  templateUrl: 'alert.html'
})

export class AlertPage {
  // search condition

  validations_form: FormGroup;
  private userId;
  public search = {
    name: "Rio de Janeiro, Brazil",
    date: new Date().toISOString()
  }

  constructor(public nav: NavController, 
    public alertService: AlertService,
    public popoverCtrl: PopoverController,
    public navParams: NavParams,
    public formBuilder: FormBuilder) {
      this.userId = navParams.get('key');
      this.createForm();


  }

  ionViewWillEnter() {
  //  this.validations_form = this.formBuilder.group({
  //   sAlertName: new FormControl(''),
    
    // nScripToken: new FormControl(''),

    // nCondition: new FormControl(''),

    // nPrice: new FormControl(''),
    
    // nUserId: new FormControl(''),

    // sVariable: new FormControl('')


    // sAlertName: [''],
    
    // nScripToken: [''],

    // nCondition:[''],

    // nPrice: [''],
    
    // // nUserId: [''],

    // sVariable: ['']
    
  // });


}
createForm(){
  this.validations_form = this.formBuilder.group({
    sAlertName: new FormControl(''),
    
    nScripToken: new FormControl(''),

    nCondition: new FormControl(''),

    nPrice: new FormControl(''),
    
    nUserId: new FormControl(this.userId),

    sVariable: new FormControl('')


    // sAlertName: [''],
    
    // nScripToken: [''],

    // nCondition:[''],

    // nPrice: [''],
    
    // nUserId: [''],

    // sVariable: ['']
    
  });

//     {
//       "sVariable":"CLOSE",
//     "nScripToken":11536,
//     "nCondition":2,
//     "nPrice":564.10,
//     "sAlertName":"TCSALERT2019",
//     "nUserId":1
//  }
    // // this.search.pickup = "Rio de Janeiro, Brazil";
    // // this.search.dropOff = "Same as pickup";
    // this.storage.get('pickup').then((val) => {
    //   if (val === null) {
    //     this.search.name = "Rio de Janeiro, Brazil"
    //   } else {
    //     this.search.name = val;
    //   }
    // }).catch((err) => {
    //   console.log(err)
    // });
  }

  // go to result page
  doSearch() {
    this.nav.push(TripsPage);
  }

  // choose place
  choosePlace(from) {
    this.nav.push(SearchLocationPage, from);
  }

  // to go account page
  goToAccount() {
    this.nav.push(SettingsPage);
  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: myEvent
    });
  }


  save(): void{
    console.log( this.validations_form.value, this.userId)
        this.alertService.save(this.validations_form.value)
                .subscribe(res => {
                 
                  console.log("data", res);
        
                  }
               
                );
              console.log("data", this.validations_form);
        
              this.nav.push(HomePage);
      
        
            }

}

//

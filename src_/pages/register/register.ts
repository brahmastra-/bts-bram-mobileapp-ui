import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {LoginPage} from "../login/login";
import {HomePage} from "../home/home";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {


  validations_form: FormGroup;
  
  constructor(public nav: NavController,      public formBuilder: FormBuilder) {
  }



  ionViewWillLoad() {
    
        this.validations_form = this.formBuilder.group({
          name: new FormControl('', Validators.compose([
            Validators.maxLength(25),
            Validators.minLength(2),
            Validators.required
          ])),
    
          mail: new FormControl('', Validators.compose([
            Validators.maxLength(25),
            Validators.minLength(2),
            Validators.required
          ])),

          mob: new FormControl('', Validators.compose([
            Validators.maxLength(25),
            Validators.minLength(2),
            Validators.required
          ])),
    
        });
        
    
      }

  // register and go to home page
  register() {
    this.nav.setRoot(LoginPage);
  }

  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }
}

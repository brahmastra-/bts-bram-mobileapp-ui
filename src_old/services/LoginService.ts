import { Component, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

export class User {
  constructor(
    public email: string,
    public password: string,
    public userType: string) { }
}

var users = [
  new User('admin.com', 'admin', 'subscriber'),
  new User('user1@gmail.com', 'a23', 'subscriber')
];

@Injectable()
export class LoginService {
   BASE_URL="http://52.66.150.242";
 // BASE_URL = Api.BASE_URL;

  constructor(public http: Http) { }

  logout() {
    localStorage.removeItem("user");
  }

  login(user) {
    var authenticatedUser = users.find(u => user.email === user.email);
    if (authenticatedUser && authenticatedUser.password === user.password) {
      localStorage.setItem("user", authenticatedUser.email);
      return true;
    }
    return false;

  }

  loginFunction(data: any) {

    let headers = new Headers({
      'Content-Type': 'application/json',
      'tenantIdHeader': 0
    });

    let options = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.BASE_URL + '/login', data, options);
  }

  loginSso(token:string, orgid:string, email:string) {
    
        let headers = new Headers({
          'Content-Type': 'application/json',
          'tenantIdHeader': 0
        });
    
        let options = new RequestOptions({ headers: headers, method: "get" });
        return this.http.get(this.BASE_URL + '/loginservice/marketplace/sso?token='+token+'&orgid='+orgid+'&email='+email , options);
      }

  checkCredentials() {
    if (localStorage.getItem("user") === null) {
    }
  }
  thirdPartyLogin(data: any) {

    let headers = new Headers({
      'Content-Type': 'application/json',
      'tenantIdHeader': 0
    });

    let options = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.BASE_URL + '/loginservice/thirdPartyLogin', data, options);
  }

}